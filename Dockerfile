FROM golang:1.20.4 as builder

RUN mkdir -p /src
COPY main.go /src
COPY go.mod /src

WORKDIR /src

RUN go build -o /app .

#FROM scratch
FROM busybox

COPY --from=builder /app /

ENTRYPOINT ["/app"]